<?php

namespace App\Controller;

use App\Entity\Post;
use App\Services\DiscountService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    protected $discountService;

    /**
     * PostController constructor.
     * @param DiscountService $discountService
     */
    public function __construct(DiscountService $discountService)
    {
        $this->discountService = $discountService;
    }

    /**
     * @Route("/post", name="post", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $posts = $this->getDoctrine()->getRepository(Post::class)->findAll();

        return $this->render('post/index.html.twig', [
            'controller_name' => 'PostController',
            'posts' => $posts,
            'random_discount' => $this->discountService->discounts
        ]);
    }

    /**
     * @Route("/post/{id}", name="post_detail", methods={"GET"}, requirements={"id":"\d+"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detail($id)
    {
        $data = [];

        $entityManager = $this->getDoctrine()->getManager();
        $conn = $entityManager->getConnection();

        $sql = $conn->prepare('select * from get_post_by_id(:id)');
        $sql->execute(['id' => $id]);
        if (!($post = $sql->fetch())) {
            return $this->json([
                'status' => 200,
                'message' => 'Data not found',
                'data' => $data
            ]);
        }

        $data = [
            'id' => $post['id'],
            'title' => $post['title'],
            'content' => $post['content'],
            'user_id' => !empty($post['user_id'])?$post['user_id']:0,
            'created_at' => trim($post['created_at']),
            'updated_at' => trim($post['updated_at']),
        ];

        return $this->json([
            'status' => 200,
            'message' => 'OK',
            'data' => $data
        ]);
    }

    /**
     * @Route("/post/add", name="post_add", methods={"POST"})
     */
    public function store()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $post = new Post;
        $post->setTitle("Another post");
        $post->setContent("New content 2020");
        $entityManager->persist($post);
        $entityManager->flush();
die;
        return $this->redirectToRoute('post_detail',
            ['id' => $post->getId()]);
    }

    /**
     * @Route("download", name="post_download", methods={"GET"})
     */
    public function download()
    {
        $path = $this->getParameter('uploaded_directory');
        return $this->file($path. 'baloon.jpg');
    }
}
