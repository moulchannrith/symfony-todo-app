<?php

namespace App\Controller;

use App\Entity\ToDo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ToDoController extends AbstractController
{
    /**
     * @Route("/todo/list", name="todo_list", methods={"GET"})
     */
    public function index()
    {
        $token = md5(uniqid(rand(), true));

        $entityManager = $this->getDoctrine()->getManager();
        $conn = $entityManager->getConnection();

        $sql = $conn->prepare('SELECT * FROM get_all_todos()');
        $sql->execute();
        $todos = $sql->fetchAll();

        return $this->render('to_do/index.html.twig', [
            'controller_name' => 'ToDoController',
            'todos' => $todos,
            'csf' => $token
        ]);
    }

    /**
     * @Route("/todo/add", name="todo_add", methods={"POST"})
     */
    public function store()
    {
        if (isset($_POST['_POST']) && !empty($_POST['task_name'])) {
            $task_name = $_POST['task_name'];
            $entityManager = $this->getDoctrine()->getManager();
            $todo = new ToDo;
            $todo->setName(htmlspecialchars($task_name, true));
            $todo->setDueDate(new \DateTime("2020-03-23 18:00:00"));
            $entityManager->persist($todo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('todo_list');
    }

    /**
     * @Route("/todo/update", name="todo_update", methods={"POST"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function update()
    {
        if (isset($_POST['_PUT']) && !empty($_POST['task_id'])) {
            $id = $_POST['task_id'];
            $status = $_POST['status'];
            $entityManager = $this->getDoctrine()->getManager();

            if ($todo = $entityManager->getRepository(ToDo::class)->find($id)) {
                $todo->setStatus($status);

                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('todo_list');
    }

    /**
     * @Route("/todo/delete", name="todo_delete", methods={"POST"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete()
    {
        if (isset($_POST['_DELETE']) && !empty($_POST['task_id'])) {
            $id = $_POST['task_id'];
            $entityManager = $this->getDoctrine()->getManager();

            if ($todo = $entityManager->getRepository(ToDo::class)->find($id)) {
                $entityManager->remove($todo);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('todo_list');
    }
}
