CREATE OR REPLACE FUNCTION get_all_todos()
   RETURNS TABLE (
      id INT,
      name VARCHAR,
      user_id INT,
      status SMALLINT,
      due_date TIMESTAMP,
      created_at TIMESTAMP,
      updated_at TIMESTAMP
)
AS $$
BEGIN
   RETURN QUERY SELECT
      t.id,
      t.name,
      t.user_id,
      t.status,
      t.due_date,
      t.created_at,
      t.updated_at
   FROM
      to_do t
   ORDER BY
   	  t.created_at ASC;
END; $$

LANGUAGE 'plpgsql';;