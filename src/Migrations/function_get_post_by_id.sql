CREATE OR REPLACE FUNCTION get_post_by_id(postid INT)
   RETURNS TABLE (
      id INT,
      title VARCHAR,
      content TEXT,
      user_id INT,
      created_at TIMESTAMP,
      updated_at TIMESTAMP
)
AS $$
BEGIN
   RETURN QUERY SELECT
      p.id,
      p.title,
      p.content,
      p.user_id,
      p.created_at,
      p.updated_at
   FROM
      post p
   where
   	  p.id = postid;
END; $$

LANGUAGE 'plpgsql';;