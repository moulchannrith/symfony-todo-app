<?php

namespace App\Services;


class DiscountService
{
    public $discounts = ['5%', '10%', '15%', '20%', '30%', '40%', '50%'];

    public function __construct()
    {
        shuffle($this->discounts);
    }
}